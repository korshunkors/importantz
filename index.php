<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ImportantZ | Будущее майнкрафт плагинов</title>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/lng.css">
</head>
<body>
<center>
<div id="languageSwitcher">
    <button onclick="switchLanguage('en')" class="en_lang_btn">English</button>
    <button onclick="switchLanguage('ru')" class="ru_lang_btn">Русский</button>
</div>
</center>
<center><b><h1 id="header-h">Documentation</h1></b></center>
<div class="commands">
    <h1 style="font-family: Arial;">Commands:</h1>
    <h1 id="importantz-command">/importantz - Main Plugin`s command</h1>
    <h1 id="clear-command">/clear - Clear Player`s inventory</h1>
    <h1 id="craft-command">/craft - Opens virtual crafting table</h1>
    <h1 id="day-command">/day - Setts day in world</h1>
    <h1 id="delhome-command">/delhome - Deletes a home point for the Player</h1>
    <h1 id="delwarp-command">/delwarp - Deletes a warp</h1>
    <h1 id="enderchest-command">/enderchest - Opens Player`s ender chest</h1>
    <h1 id="feed-command">/feed - Feeds the Player</h1>
    <h1 id="fly-command">/fly - Enable/Disable fly mode</h1>
    <h1 id="flyspeed-command">/flyspeed - Setts fly speed for Player</h1>
    <h1 id="gm-command">/gm - Changes Player`s game mode</h1>
    <h1 id="heal-command">/heal - Heals Player</h1>
    <h1 id="home-command">/home - Teleports the Player to a point at home</h1>
    <h1 id="invsee-command">/invsee - View Player`s inventory</h1>
    <h1 id="lightning-command">/lightning - To strike with lightning</h1>
    <h1 id="near-command">/near - View the list of Players nearby</h1>
    <h1 id="nick-command">/nick - Changes Player`s nick</h1>
    <h1 id="night-command">/night - Setts night in world</h1>
    <h1 id="realname-command">/realname - View real Player`s name</h1>
    <h1 id="repair-command">/repair - Repairs item in Player`s hand</h1>
    <h1 id="sethome-command">/sethome - Creates a home point for the Player</h1>
    <h1 id="setspawn-command">/setspawn - Creates a spawn point</h1>
    <h1 id="setwarp-command">/setwarp - Creates a warp</h1>
    <h1 id="spawn-command">/spawn - Teleports the Player to a spawn point</h1>
    <h1 id="speed-command">/speed - Setts walk speed for Player</h1>
    <h1 id="tpacancel-command">/tpacancel - Cancel the teleportation request</h1>
    <h1 id="tpaccept-command">/tpaccept - Accept a teleportation request</h1>
    <h1 id="tpadeny-command">/tpadeny - Rejects teleportation request</h1>
    <h1 id="tpa-command">/tpa - Send a teleportation request</h1>
    <h1 id="tp-command">/tp - Teleport to Player</h1>
    <h1 id="tphere-command">/tphere - Teleports Player to yourself</h1>
    <h1 id="vanish-command">/vanish - Makes Player invisible</h1>
    <h1 id="warp-command">/warp - Teleports Player to warp</h1>
</div>
    <script src="js/script.js"></script>
</body>
</html>